import React, { useState, useEffect } from 'react';
import Temporizador from './components/Temporizador';
function App() {
  const datosInicio = {
    tiempo: 0,
    titulo: 'Prepara',
  };
  // const urldescanzo = 'http://streaming.tdiradio.com:8000/house.mp3';
  // const audioDescanso = new Audio(urldescanzo);
  const [cortoLargo, setCortoLargo] = useState('tiempo25');
  const tiempoT =
    cortoLargo === 'tiempo25'
      ? {
          tiempo: 24,
          titulo: 'Estudio 25 minutos',
        }
      : {
          tiempo: 44,
          titulo: 'Estudio 45 minutos',
        };
  const descanzoT =
    cortoLargo === 'tiempo25'
      ? { tiempo: 4, titulo: 'Descanso' }
      : { tiempo: 9, titulo: 'Descanso' };

  const [datostemp, setDatostemp] = useState(datosInicio);
  const [termino, setTermino] = useState(0);
  //empieza muestra el componenteM
  const [empieza, setEmpieza] = useState(false);
  const [fondo, setFondo] = useState('bg-light');
  const [clasebtn, setClasebtn] = useState('d-block');
  const handleEmpieza = () => {
    //setTerminog(1);
    // audioDescanso.play();
    console.log('empieza');
    setFondo('bg-light');
    setEmpieza(true);
    // audioDescanso.pause();

  };
  useEffect(() => {
    console.log('siguiente:**** ', termino);
    console.log('-*/-*/', empieza);

    setEmpieza(false);
    console.log('-*/-*/', empieza);
    if (empieza) {
      console.log('siguiente:---- ', termino);
      setClasebtn('d-none');

      if (termino === 1) {
        setDatostemp(tiempoT);
        console.log(datostemp);
        setFondo('bg-info');

        setTimeout(() => {
          setEmpieza(true);
        }, 500);
      }

      if (termino === 2) {
        // audioDescanso.play();
        setDatostemp(descanzoT);
        console.log(datostemp);
        setFondo('bg-warning');

        setTimeout(() => {
          setEmpieza(true);
        }, 500);
      }
      if (termino === 3) {
        setDatostemp({
          tiempo: 4,
          titulo: 'Repaso',
        });
        console.log(datostemp);
        setFondo('bg-primary');

        setTimeout(() => {
          setEmpieza(true);
        }, 500);
        // audioDescanso.pause();
      }
      if (termino === 4) {
        setEmpieza(false);
        console.log('reseteo cntador');
        setTermino(0);
        setFondo('bg-light');
        setClasebtn('d-block');

        setDatostemp(datosInicio);
        setTimeout(() => {
          setEmpieza(false);
          window.location.reload(true);
        }, 500);
      }
    }
  }, [termino, datostemp]);
  return (
    <div className={`container-fluid ${fondo}`}>
      <div className=" row">
        <div className={`jumbotron col-12 text-center bg-transparent`}>
          <h1 className="display-4">Ciclo de Estudio</h1>
          <p className="lead">
            Ciclo de estudio de 25 o 45 minutos con descanso
          </p>

          <hr className="my-4" />
          <p>
            {cortoLargo === 'tiempo25'
              ? 'Seleccionado 25 minutos'
              : 'Seleccionado 45 minutos'}
          </p>
          <div className={`col-12 text-center ${clasebtn}`}>
          <input
            type="radio"
            name="tiempo"
            id="tiempo25"
            value="tiempo25"
            onChange={() => setCortoLargo('tiempo25')}
            checked={cortoLargo === 'tiempo25'}
          />
          <label htmlFor="tiempo25">25 minutos</label>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <input
            type="radio"
            name="tiempo"
            id="tiempo45"
            value="tiempo45"
            onChange={() => setCortoLargo('tiempo45')}
            checked={cortoLargo === 'tiempo45'}
          />
          <label htmlFor="tiempo45">45 minutos</label>
        </div>
        </div>
      </div>
      <div className="row justify-content-center">
        
        <button
          type="button"
          className={`btn btn-primary ${clasebtn}`}
          onClick={handleEmpieza}
        >
          Empezar
        </button>
        {empieza ? (
          <Temporizador
            setTermino={setTermino}
            termino={termino}
            empieza={empieza}
            datostemp={datostemp}
          />
        ) : (
          <div className="card bg-transparent">
            <div className="card-body">
              <h3 className="text-center">Prepara</h3>
              <ul className="pl-3">
                <li>Agua</li>
                <li>Aseo</li>
                <li>Apuntes</li>
                <li>Música</li>
                <li>5 respiraciones</li>
                <li>MANDARINA</li>
              </ul>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default App;
