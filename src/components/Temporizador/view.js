import React, { useState, useEffect } from 'react';
import pitido from '../../pitido.mp3';
// import 'xfiles.mid';

const Temporizador25 = ({ setTermino, termino, empieza, datostemp }) => {
  const { tiempo, titulo } = datostemp;
  const segundos = termino === 0 ? 5 : 59;
  const [contadorsegundo, setContadorsegundo] = useState(segundos);
  const [contadorminuto, setContadorminuto] = useState(tiempo);
  
  const urlfin = 'https://s3.amazonaws.com/freecodecamp/simonSound1.mp3';
  const audiofin = new Audio(urlfin);

  useEffect(() => {
    const temporizar = () => {
      if (contadorsegundo === 0 && contadorminuto === 0) {
        setTermino(termino + 1);
      } else {
        setTimeout(() => {
          setContadorsegundo(contadorsegundo - 1);
          if (contadorsegundo === 0) {
            setContadorminuto(contadorminuto - 1);
            setContadorsegundo(segundos);
          }
        }, 1000);
      }

      if (contadorsegundo < 3 && contadorminuto === 0) {
        audiofin.play();
      }
    };
    if (empieza) temporizar();
    //slin
  }, [contadorsegundo, empieza]);

  return (
    <>
      <div className="card bg-transparent">
        <div className="card-body">
          <h3 className="text-center">{titulo}</h3>
          {termino === 0 ? (
            <ul className="pl-3">
              <li>Agua</li>
              <li>Aseo</li>
              <li>Apuntes</li>
              <li>Música</li>
              <li>5 respiraciones</li>
              <li>MANDARINA</li>
            </ul>
          ) : null}

          <p className="card-text text-center h1 text-bold">
            {`${contadorminuto}`.length === 1 ? 0 : null}
            {contadorminuto}:{`${contadorsegundo}`.length === 1 ? 0 : null}
            {contadorsegundo}
          </p>
        </div>
      </div>
      <div className="stage">
        <audio autoPlay>
          <source src="../../pitido.mp3" type="audio/mpeg"></source>
        </audio>
      </div>
    </>
  );
};

export default Temporizador25;
